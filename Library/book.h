#ifndef BOOK_H
#define BOOK_H
#include <string>

using namespace std;

class book
{
private :

    string bookname_;
    string authorname_;

public:
    book();
    void enter () ;

};

#endif // BOOK_H
