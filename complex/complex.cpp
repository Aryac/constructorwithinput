#include "complex.h"
#include <iostream>
#include <iomanip>
using namespace std;

complex::complex()
{
    real_=0;
    imag_=0;
}

void complex::enter()
{

    cout<<"Enter real number :";
    cin>>real_;
    cout<<"Enter imag number :";
    cin >> imag_;
    cout<<endl;
}

complex complex::operator- (complex &s1)
{

        complex temp;
        temp.real_ = (real_) - (s1.real_);
        temp.imag_ = (imag_) - (s1.imag_);
        return temp;

}

complex complex::operator+ (complex &s2)
{
        complex temp;
        temp.real_ = (real_) + (s2.real_);
        temp.imag_ = (imag_) + (s2.imag_);
        return temp;
}

complex complex::operator/ (complex &s3)
{
     complex temp;
     temp.real_=((real_*s3.real_)+(imag_*s3.imag_))/((s3.real_*s3.real_)+(s3.imag_*s3.imag_));
     temp.imag_=((imag_*s3.real_)-(real_*s3.imag_))/((s3.real_*s3.real_)+(s3.imag_*s3.imag_));
     return temp;
}

bool complex::operator== (complex &s4)
{

    if((real_ == s4.real_ && imag_ == s4.imag_))
        return true;
    else
        return false;
}

void complex::printI()
{
    cout << real_;
    if (imag_>0)
        cout<<'+'<<imag_<<'i'<<endl;

    else if (imag_<0)
        cout <<imag_<<'i'<<endl;
}

void complex::show()
{
   complex operand1,operand2,object;


    char word='y';
    char oprator;
    operand1.enter();
    operand2.enter();
    cout<<endl;

    while(word=='y')
    {
        cout<<"( Enter the operator you want (+ , - , / , == ) \n" ;
        cin>>oprator;

        switch (oprator)
        {


        case '=':
         {
             if (operand1==operand2)
             {
                 cout<<"equal\n";
             }
             else
             {
                 cout<<"not equal.\n";
             }
             break;
        }


        case '/':

            object=operand1/operand2;
            object.printI();
            break;

        case '-':

           object=operand1-operand2;
           object.printI();
           break;

        case '+':

            object=operand1+operand2;
            object.printI();
            break;

        default:

            break;
        }

        cout<<"Do you want again? \n";
        cin>>word;
        cout<<endl;
    }



}
