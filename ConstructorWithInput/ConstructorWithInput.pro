TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    funcConstructor.cpp

HEADERS += \
    funcConstructor.h
